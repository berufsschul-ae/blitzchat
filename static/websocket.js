var ws;

function send(e){
    e.preventDefault();
    const msg = {
        event: 'msg',
        msg: document.getElementById('text').value,
        username: document.getElementById('username').value,
        color: document.getElementById('color').value
    };
    ws.send(JSON.stringify(msg));
    document.getElementById('text').value = '';
}


function join(e) {
    e.preventDefault();

    document.getElementById('joinButton').remove();
    document.getElementById('username').setAttribute("readonly", "");

    ws = new WebSocket('ws://' + window.location.hostname + ':8082');
    ws.onopen = () => { 
        const msg = {
            event: 'joining',
            msg: '',
            username: document.getElementById('username').value,
            color: 'black'
        }
        const msgString = JSON.stringify(msg);
        ws.send(msgString);
    }

    ws.onmessage = (msg) => {
        const jsonObj = JSON.parse(msg.data);

        const chat = document.getElementById('chat');

        if(jsonObj.event === 'joining') {
            chat.innerHTML += getMessageSpan(jsonObj, jsonObj.username + ' has joined the chat!');
        } else {
            chat.innerHTML += getTime(new Date(jsonObj.timestamp)) + '<span style="font-weight: bold">' + jsonObj.username + '</span>' + ': ' + getMessageSpan(jsonObj, jsonObj.msg);
        }
        console.log(msg.data);

        // scroll to the bottom of the chat
        chat.scrollTop = chat.scrollHeight;
    };

    ws.onclose = (msg) => {
        const chat = document.getElementById('chat');
        chat.innerHTML += '<b>Connection Closed</b>';
    }

    function getTime(date) {
        let hours = date.getHours();
        let minutes = date.getMinutes();
        if(hours < 10) {
            hours = "0" + hours;
        }
        if(minutes < 10) {
            minutes = "0" + minutes;
        }
        return '<span style="color: lightgray; font-weight: lighter" >' + hours + ":" + minutes + ' </span>';
    }

    function getMessageSpan(jsonObj, innerMessage){
        return '<span style="color:' + jsonObj.color + '; font-weight: ' + jsonObj.style + '" >' + innerMessage + '</span>' + '<br>'
    }
}