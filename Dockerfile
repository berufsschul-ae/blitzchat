FROM node:12.13.1-buster

COPY . /tmp/app/
WORKDIR /tmp/app/

RUN npm install

EXPOSE 8081/tcp
EXPOSE 8082/tcp

CMD ["npm", "start"]